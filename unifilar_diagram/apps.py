from django.apps import AppConfig


class UnifilarDiagramConfig(AppConfig):
    name = 'unifilar_diagram'
